// ======== Code ========

// TODO: Create a calculator!

angular.module('app', [])
  .component('calculator', {
    controller: CalculatorController,
    template: function() {
      return `
        <div>
          <div>{{$ctrl.currentEnteredValue || $ctrl.result || 0}}</div>
          <div>{{$ctrl.operation}}</div>
        <div>
        <div>
          <button ng-click="$ctrl.updateNumber(1)">1</button>
          <button ng-click="$ctrl.updateNumber(2)">2</button>
          <button ng-click="$ctrl.updateNumber(3)">3</button>
          <button ng-click="$ctrl.divide()">/</button>
        </div>
        <div>
          <button ng-click="$ctrl.updateNumber(4)">4</button>
          <button ng-click="$ctrl.updateNumber(5)">5</button>
          <button ng-click="$ctrl.updateNumber(6)">6</button>
          <button ng-click="$ctrl.multiply()">*</button>
        </div>
        <div>
          <button ng-click="$ctrl.updateNumber(7)">7</button>
          <button ng-click="$ctrl.updateNumber(8)">8</button>
          <button ng-click="$ctrl.updateNumber(9)">9</button>
          <button ng-click="$ctrl.minus()">-</button>
        </div>
        <div>
          <button ng-click="$ctrl.updateNumber(0)">0</button>
          <button ng-click="$ctrl.updateNumber('.')">.</button>
          <button ng-click="$ctrl.equal('.')">=</button>
          <button ng-click="$ctrl.plus()">+</button>
        </div>
      `;
    }
  })

  function CalculatorController() {
    var vm = this;

    vm.currentEnteredValue = '';
    vm.operation = '';
    vm.result = null

    function makeCalculation() {
      switch (vm.operation) {
        case '+':
          vm.result = parseFloat(vm.result) + parseFloat(vm.currentEnteredValue);
          break;
        case '-':
          vm.result = parseFloat(vm.result) - parseFloat(vm.currentEnteredValue);
          break;
        case '*':
          vm.result = parseFloat(vm.result) * parseFloat(vm.currentEnteredValue);
          break;
        case '/':
          vm.result = parseFloat(vm.result) / parseFloat(vm.currentEnteredValue);
          break;
      }
    }

    vm.plus = function() {
      if (vm.operation && vm.currentEnteredValue) {
        makeCalculation();
      }

      vm.operation = '+';
      vm.currentEnteredValue = '';
    }

    vm.minus = function() {
      if (vm.operation && vm.currentEnteredValue) {
        makeCalculation();
      }

      vm.operation = '-';
      vm.currentEnteredValue = '';
    }

    vm.multiply = function() {
      if (vm.operation && vm.currentEnteredValue) {
        makeCalculation();
      }

      vm.operation = '*';
      vm.currentEnteredValue = '';
    }

    vm.divide = function() {
      if (vm.operation && vm.currentEnteredValue) {
        makeCalculation();
      }

      vm.operation = '/';
      vm.currentEnteredValue = '';
    }

    vm.equal = function() {
      if (vm.operation && vm.currentEnteredValue) {
        makeCalculation();
      }

      vm.operation = '';
      vm.currentEnteredValue = '';
    }


    vm.updateNumber = function(value) {
      if ((value !== '.') || vm.currentEnteredValue.indexOf('.') === -1) {
        vm.currentEnteredValue += value.toString()
      }

      if (!vm.operation) {
        vm.result = vm.currentEnteredValue;
      }
    }
  }

// ======== specs ========

describe("Dummy true", function() {
  it("should be true", function() {
    expect(true).toBe(true);
  })
})

// ======== Jasmine ========
//
// (function() {
//  var env = jasmine.getEnv();
//  env.addReporter(new jasmine.HtmlReporter());
//  env.execute();
// }())
